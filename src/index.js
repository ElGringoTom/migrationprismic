import AdmZip from 'adm-zip';
import {mkdirSync, readFileSync, writeFileSync, rmdirSync, readdir} from 'fs';

if (process.argv.length <= 2) {
    console.log("Error : Prismic document archive path is missing\nUsage : npm run start archive.zip");
    process.exit();
}

const source = process.argv[2];
const target = process.cwd() + '\\tmp';
const outputDir = process.cwd() + '\\output'

const documentFilters = {
    'media_presse_v2': {
        filter: (document) => document.category === 'Communiqué de presse',
        mapper: (inputJson) => {
            console.log("media presse mapper");
            return inputJson;
        }
    },
    'article_v2': {
        filter: (document) => true,
        mapper: (inputJson) => {
            console.log("article mapper");
            return inputJson;
        }
    }
}

const extractSource = async (source, target) => {
    console.log("Removing temporary directory ...");
    rmdirSync(target, {recursive: true});
    console.log(`Extracting files ...`);
    try {
        const zip = new AdmZip(source);
        zip.extractAllTo(target, true);
        console.log(`Extraction complete to ${target}`)
    } catch (err) {
        console.log('An error occured while extracting files : ', err.message)
    }
}

const migrate = async (source, target) => {
    await extractSource(source, target);
    console.log("Emptying output directory ...");
    rmdirSync(outputDir, {recursive: true});
    mkdirSync(outputDir);

    console.log("Parsing files ...");
    readdir(target, (err, files) => {
        files.map((file, index) => {
            const fileData = readFileSync('./tmp/' + file);
            const fileJson = JSON.parse(fileData);
            const path = `${outputDir}\\migrated-${index}-${file}`;
            if (documentFilters[fileJson.type] && documentFilters[fileJson.type].filter(fileJson)) {
                const migratedJson = documentFilters[fileJson.type].mapper(fileJson);
                console.log(`Writing files ${path} ...`);
                writeFileSync(path, JSON.stringify(migratedJson));
            }
        });
    });

    console.log(`Creating output zip ...`);

    const targetZip = new AdmZip();
    targetZip.addLocalFolder(outputDir, `${process.cwd()}\\output.zip`);
    targetZip.writeZip(`${process.cwd()}\\output.zip`);
}

migrate(source, target);


